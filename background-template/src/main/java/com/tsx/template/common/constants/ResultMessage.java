package com.tsx.template.common.constants;

import lombok.Data;

@Data
public class ResultMessage {
    public static final String LOGIN_ERROR = "用户名或密码错误";
    public static final String LOGIN_LOCK = "当前用户已被禁用,请联系管理员";
    public static final String NOT_ACTION = "当前数据是系统数据,不能操作";
    public static final String DEL_ROLE_ERROR = "当前角色绑定着用户，不能删除";
    public static final String NOT_ACTION_USER = "不能对当前登录用户进行操作";
}
