package com.tsx.template.common.constants;

public final class StatusCode {
    // 成功
    public static final int SUCCESS = 2000;
    // 登录异常
    public static final int LOGIN_ERROR = 4001;
    // 权限
    public static final int PERMISSION_ERROR = 4003;
    // 参数异常
    public static final int PARAM_ERROR = 4005;
    // 错误
    public static final int ERROR = 5000;


    private StatusCode() {
        // 私有构造函数，防止实例化
    }
}
