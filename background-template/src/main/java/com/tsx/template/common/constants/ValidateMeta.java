package com.tsx.template.common.constants;

import com.tsx.template.common.exception.BusinessException;

import java.util.List;

public class ValidateMeta {
    public static final int ROLE_ID_ADMIN = 1;
    public static final long USER_ID_ADMIN = 1;

    // 校验单个角色
    public static void validateRole(int id) {
        if (id == ROLE_ID_ADMIN) {
            throw new BusinessException(StatusCode.ERROR, ResultMessage.NOT_ACTION);
        }
    }

    // 校验单个用户
    public static void validateUser(Long id) {
        if (id == USER_ID_ADMIN) {
            throw new BusinessException(StatusCode.ERROR, ResultMessage.NOT_ACTION);
        }
    }

    // 校验多个用户
    public static void validateUser(List<Long> ids){
        if(ids.contains(USER_ID_ADMIN)){
            throw new BusinessException(StatusCode.ERROR,ResultMessage.NOT_ACTION);
        }
    }

    public static void validateRole(List<Integer> ids){
        if(ids.contains(ROLE_ID_ADMIN)){
            throw new BusinessException(StatusCode.ERROR,ResultMessage.NOT_ACTION);
        }
    }
    private ValidateMeta() {
    }
}
