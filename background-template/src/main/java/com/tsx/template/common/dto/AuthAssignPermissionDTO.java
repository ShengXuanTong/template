package com.tsx.template.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthAssignPermissionDTO {
    // 角色id
    private Integer roleId;
    // 权限id集合
    private List<Integer> permissions;
}
