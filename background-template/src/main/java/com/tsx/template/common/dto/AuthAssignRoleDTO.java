package com.tsx.template.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthAssignRoleDTO {
    // 用户id
    private Long userId;
    // 角色id
    private List<Integer> roles;
}
