package com.tsx.template.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthUserUpdateDTO {
    private Long id;
    private String nickName;
    private Integer status;
    private String avatar;
    private Integer[] roles;
}
