package com.tsx.template.common.utils;

import com.tsx.template.common.vo.PermissionVO;
import com.tsx.template.entity.AuthPermission;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TreeUtil {
    private TreeUtil() {
    }

    public static List<PermissionVO> TreeMenu(List<AuthPermission> permissions, Integer parentId) {
        List<PermissionVO> menuVOList = new ArrayList<>();
        for (AuthPermission permission : permissions) {
            if (Objects.equals(permission.getParentId(), parentId)) {
                PermissionVO menuVO = new PermissionVO();
                menuVO.setName(permission.getName());
                menuVO.setId(permission.getId());
                if (permission.getType() == 1) {
                    menuVO.setCode(permission.getCode());
                } else {
                    menuVO.setCode(permission.getMenuUrl());
                }
                // 递归获取子菜单
                menuVO.setChildren(TreeMenu(permissions, permission.getId()));

                menuVOList.add(menuVO);
            }
        }
        return menuVOList;
    }
}
