package com.tsx.template.common.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthUserInfoVO {
    public List<String> routes;
    public List<String> buttons;
    public List<String> roles;
    public String nickName;
    public String avatar;
}
