package com.tsx.template.common.vo;

import com.tsx.template.entity.AuthRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthUserRoleVO {
    private Long id;
    private String username;
    private String nickName;
    private Integer status;
    private String avatar;
    private List<AuthRole> roles;
}
