package com.tsx.template.common.vo;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class PermissionVO {
    private Integer id;
    private String name;
    private String code;
    private List<PermissionVO> children;
}
