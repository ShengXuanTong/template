package com.tsx.template.common.vo;

import cn.dev33.satoken.util.SaResult;
import com.tsx.template.common.constants.StatusCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result<T> implements Serializable {
    // 状态码
    private Integer code;
    // 数据
    private T data;
    // 错误信息
    private String message;

    public static <T> Result<T> ok() {
        return new Result<>(StatusCode.SUCCESS, null, "ok");
    }

    public static <T> Result<T> data(T data) {
        return new Result<>(StatusCode.SUCCESS, data, "ok");
    }

    public static <T> Result<T> msg(String message) {
        return new Result<>(StatusCode.SUCCESS, null, message);
    }

    public static <T> Result<T> error(String message) {
        return new Result<>(StatusCode.ERROR, null, message);
    }

    public static <T> Result<T> error(int code, String message) {
        return new Result<>(code, null, message);
    }

    public static <T> Result<T> custom(int code, T data, String message) {
        return new Result<>(code, data, message);
    }
}
