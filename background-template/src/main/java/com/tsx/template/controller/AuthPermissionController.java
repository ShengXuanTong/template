package com.tsx.template.controller;

import com.tsx.template.common.utils.TreeUtil;
import com.tsx.template.common.vo.AuthPermissionVO;
import com.tsx.template.common.vo.PermissionVO;
import com.tsx.template.common.vo.Result;
import com.tsx.template.entity.AuthPermission;
import com.tsx.template.service.AuthPermissionService;
import com.tsx.template.service.AuthRolePermissionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/permission")
@RequiredArgsConstructor
public class AuthPermissionController {
    final AuthPermissionService authPermissionService;
    final AuthRolePermissionService authRolePermissionService;

    @GetMapping
    public Result<List<PermissionVO>> getPermissions() {
        List<AuthPermission> permissions = authPermissionService.list();
        List<PermissionVO> permissionVOS = TreeUtil.TreeMenu(permissions, 0);
        return Result.data(permissionVOS);
    }

    @GetMapping("/{roleId}")
    public Result<AuthPermissionVO> queryPermissionsByRoleId(@PathVariable int roleId) {
        AuthPermissionVO authPermissionVOS = authRolePermissionService.queryPermissionsByRoleId(roleId);
        return Result.data(authPermissionVOS);
    }

}
