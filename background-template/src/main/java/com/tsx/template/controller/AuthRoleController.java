package com.tsx.template.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tsx.template.common.constants.ValidateMeta;
import com.tsx.template.common.dto.AuthAssignPermissionDTO;
import com.tsx.template.common.vo.AuthPermissionVO;
import com.tsx.template.common.vo.AuthUserRoleVO;
import com.tsx.template.common.vo.PageInfoResult;
import com.tsx.template.common.vo.Result;
import com.tsx.template.entity.AuthRole;
import com.tsx.template.service.AuthRoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/role")
@RequiredArgsConstructor
@Slf4j
public class AuthRoleController {
    final AuthRoleService authRoleService;

    @GetMapping("/{page}/{limit}")
    public Result<PageInfoResult<AuthRole>> pageRole(@PathVariable int page, @PathVariable int limit, String roleName) {
        IPage<AuthRole> iPage = new Page<>(page, limit);
        LambdaQueryWrapper<AuthRole> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(StringUtils.hasText(roleName), AuthRole::getRemark, roleName);
        IPage<AuthRole> pageInfo = authRoleService.page(iPage, wrapper);
        PageInfoResult<AuthRole> pageInfoResult = new PageInfoResult<>();
        pageInfoResult.setTotal(pageInfo.getTotal());
        pageInfoResult.setRecords(pageInfo.getRecords());
        pageInfoResult.setCurrent(pageInfo.getCurrent());
        return Result.data(pageInfoResult);
    }

    // 查看所有角色信息
    @GetMapping
    public Result<List<AuthRole>> getAllRole() {
        List<AuthRole> roles = authRoleService.list();
        return Result.data(roles);
    }

    @PostMapping
    @SaCheckPermission(value = "btn.role.add")
    public Result<String> addRole(@RequestBody AuthRole authRole) {
        authRoleService.save(authRole);
        return Result.ok();
    }

    @DeleteMapping("/{id}")
    @SaCheckPermission(value = "btn.role.delete")
    public Result<String> removeById(@PathVariable int id) {
        ValidateMeta.validateRole(id);
        // 要检查对应的角色是否有用户绑定
        authRoleService.deleteWithUser(id);
        return Result.ok();
    }

    @PutMapping
    @SaCheckPermission(value = "btn.role.update")
    public Result<String> updateById(@RequestBody AuthRole authRole) {
        ValidateMeta.validateRole(authRole.getId());
        authRoleService.updateById(authRole);
        return Result.ok();
    }

    @PutMapping("/assignPermission")
    @SaCheckPermission(value = "btn.role.assign")
    public Result<String> assignPermission(@RequestBody AuthAssignPermissionDTO authAssignPermissionDTO) {
        ValidateMeta.validateRole(authAssignPermissionDTO.getRoleId());
        log.info("给角色:{}分配权限:{}", authAssignPermissionDTO.getRoleId(), authAssignPermissionDTO.getPermissions());
        authRoleService.assignPermission(authAssignPermissionDTO);
        return Result.ok();
    }

    @GetMapping("/{id}")
    public Result<AuthRole> queryById(@PathVariable int id) {
        AuthRole authRole = authRoleService.getById(id);
        return Result.data(authRole);
    }

}
