package com.tsx.template.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.stp.StpUtil;
import com.tsx.template.common.constants.ResultMessage;
import com.tsx.template.common.constants.StatusCode;
import com.tsx.template.common.constants.ValidateMeta;
import com.tsx.template.common.dto.AuthAssignRoleDTO;
import com.tsx.template.common.dto.AuthUserAddDTO;
import com.tsx.template.common.dto.AuthUserLoginDTO;
import com.tsx.template.common.dto.AuthUserUpdateDTO;
import com.tsx.template.common.exception.BusinessException;
import com.tsx.template.common.vo.*;
import com.tsx.template.service.AuthUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
@Slf4j
public class AuthUserController {
    final AuthUserService authUserService;

    @PostMapping("/login")
    public Result<AuthUserLoginVO> login(@RequestBody AuthUserLoginDTO authUserLoginDTO) {
        log.info("登录用户:{}", authUserLoginDTO);
        AuthUserLoginVO login = authUserService.login(authUserLoginDTO);
        return Result.data(login);
    }

    @GetMapping("/logout")
    public Result<String> logout() {
        log.info("退出登录");
        StpUtil.logout();
        return Result.ok();
    }

    @GetMapping("/info")
    public Result<AuthUserInfoVO> info() {
        AuthUserInfoVO authUserInfoVO = authUserService.info();
        return Result.data(authUserInfoVO);
    }

    @GetMapping("/{page}/{limit}")
    public Result<PageInfoResult<AuthUserPageItemVO>> pageUser(@PathVariable int page, @PathVariable int limit, String username) {
        PageInfoResult<AuthUserPageItemVO> result = authUserService.pageUser(page, limit, username);
        return Result.data(result);
    }

    @PutMapping
    @SaCheckPermission(value = "btn.user.update")
    public Result<String> updateUser(@RequestBody AuthUserUpdateDTO authUserUpdateDTO) {
        ValidateMeta.validateUser(authUserUpdateDTO.getId());
        authUserService.updateUserWithRole(authUserUpdateDTO);
        return Result.ok();
    }

    @DeleteMapping
    @SaCheckPermission(value = "btn.user.delete")
    public Result<String> removeById(@RequestBody List<Long> ids) {
        if (ids.contains(ValidateMeta.USER_ID_ADMIN)) {
            throw new BusinessException(StatusCode.ERROR, ResultMessage.NOT_ACTION);
        }
        log.info("删除的id有:{}", ids);
        authUserService.removeUserAndRoles(ids);
        return Result.ok();
    }

    @PostMapping
    @SaCheckPermission(value = "btn.user.add")
    public Result<String> addUser(@Valid @RequestBody AuthUserAddDTO authUserAddDTO) {
        authUserService.addUser(authUserAddDTO);
        return Result.ok();
    }

    @PutMapping("/assignRole")
    public Result<String> assignRole(@RequestBody AuthAssignRoleDTO authAssignRoleDTO) {
        ValidateMeta.validateUser(authAssignRoleDTO.getUserId());
        log.info("给用户:{}赋予角色:{}", authAssignRoleDTO.getUserId(), authAssignRoleDTO.getRoles());
        authUserService.assignRole(authAssignRoleDTO);
        return Result.ok();
    }

    @GetMapping("/{userId}")
    public Result<AuthUserRoleVO> getUserWithRoles(@PathVariable Long userId) {
        AuthUserRoleVO authUserRoleVO = authUserService.queryUserWithRoles(userId);
        return Result.data(authUserRoleVO);
    }

    @PutMapping("/resetPwd/{id}")
    public Result<String> resetPwd(@PathVariable Long id) {
        ValidateMeta.validateUser(id);
        authUserService.resetPwd(id);
        return Result.ok();
    }
}
