package com.tsx.template.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaIgnore;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tsx.template.common.vo.PageInfoResult;
import com.tsx.template.common.vo.Result;
import com.tsx.template.entity.Brand;
import com.tsx.template.service.BrandService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/product/brand")
@RequiredArgsConstructor
public class BrandController {
    final BrandService brandService;

    @GetMapping("/{page}/{limit}")
    public Result<PageInfoResult<Brand>> page(@PathVariable int limit, @PathVariable int page) {
        IPage<Brand> iPage = new Page<>(page, limit);
        brandService.page(iPage);
        PageInfoResult<Brand> pageInfoResult = new PageInfoResult<>();
        pageInfoResult.setCurrent(iPage.getCurrent());
        pageInfoResult.setTotal(iPage.getTotal());
        pageInfoResult.setRecords(iPage.getRecords());
        return Result.data(pageInfoResult);
    }

    @GetMapping("/{id}")
    @SaCheckPermission("btn.brand.select")
    public Result<Brand> queryById(@PathVariable Long id) {
        Brand brand = brandService.getById(id);
        return Result.data(brand);
    }

    @PutMapping
    @SaCheckPermission("btn.brand.update")
    public Result<String> updateByID(@RequestBody Brand brand) {
        brandService.updateById(brand);
        return Result.ok();
    }

    @PostMapping
    @SaCheckPermission("btn.brand.add")
    public Result<String> save(@RequestBody Brand brand) {
        brandService.save(brand);
        return Result.ok();
    }

    @DeleteMapping("/{id}")
    @SaCheckPermission("btn.brand.delete")
    public Result<String> delete(@PathVariable Long id) {
        brandService.removeById(id);
        return Result.ok();
    }
}
