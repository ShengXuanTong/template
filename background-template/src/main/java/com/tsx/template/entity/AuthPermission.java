package com.tsx.template.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;

/**
 * 权限
 *
 * @TableName auth_permission
 */
@TableName(value = "auth_permission")
@Data
public class AuthPermission implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 权限名字
     */
    private String name;

    /**
     * 父id
     */
    private Integer parentId;

    /**
     * 权限类型 0菜单 1操作
     */
    private Integer type;

    /**
     * 操作标识符
     */
    private String code;

    /**
     * 菜单路由
     */
    private String menuUrl;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}