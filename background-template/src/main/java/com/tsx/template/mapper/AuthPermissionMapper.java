package com.tsx.template.mapper;

import com.tsx.template.entity.AuthPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Xuan
 * @description 针对表【auth_permission(权限)】的数据库操作Mapper
 * @createDate 2024-04-03 12:04:02
 * @Entity com.tsx.template.entity.AuthPermission
 */
public interface AuthPermissionMapper extends BaseMapper<AuthPermission> {

    // 根据用户id获取按钮权限
    List<AuthPermission> getPermissionsByUserId(@Param(value = "id") Long loginId);
}




