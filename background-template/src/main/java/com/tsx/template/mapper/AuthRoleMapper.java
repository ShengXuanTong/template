package com.tsx.template.mapper;

import com.tsx.template.entity.AuthRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tsx.template.entity.AuthUserRole;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Xuan
 * @description 针对表【auth_role(角色)】的数据库操作Mapper
 * @createDate 2024-04-03 12:04:02
 * @Entity com.tsx.template.entity.AuthRole
 */
public interface AuthRoleMapper extends BaseMapper<AuthRole> {

    List<AuthRole> getRolesByUserId(@Param(value = "id") Long loginId);
}




