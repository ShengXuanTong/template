package com.tsx.template.mapper;

import com.tsx.template.entity.AuthRolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author Xuan
 * @description 针对表【auth_role_permission(角色权限关联表)】的数据库操作Mapper
 * @createDate 2024-04-03 12:04:02
 * @Entity com.tsx.template.entity.AuthRolePermission
 */
public interface AuthRolePermissionMapper extends BaseMapper<AuthRolePermission> {

    @Delete("delete from auth_role_permission where role_id = #{roleId}")
    void deleteByRoleId(Integer roleId);

    void insertBatchRolePermission(@Param("rolePermissions") List<AuthRolePermission> rolePermissions);

    @Select("select permission_id from auth_role_permission where role_id = #{roleId}")
    List<Integer> queryPermissionsByRoleId(@Param("roleId") int roleId);
}




