package com.tsx.template.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.tsx.template.common.vo.AuthUserPageItemVO;
import com.tsx.template.entity.AuthUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author Xuan
 * @description 针对表【auth_user】的数据库操作Mapper
 * @createDate 2024-04-03 12:04:22
 * @Entity com.tsx.template.entity.AuthUser
 */
public interface AuthUserMapper extends BaseMapper<AuthUser> {

    IPage<AuthUserPageItemVO> pageUserWithRoles(IPage<AuthUserPageItemVO> iPage, @Param("username") String username);
}




