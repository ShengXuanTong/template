package com.tsx.template.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tsx.template.common.vo.AuthUserRoleVO;
import com.tsx.template.entity.AuthUserRole;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author Xuan
 * @description 针对表【auth_user_role(用户角色表)】的数据库操作Mapper
 * @createDate 2024-04-07 14:20:03
 * @Entity com.tsx.template.entity.AuthUserRole
 */
public interface AuthUserRoleMapper extends BaseMapper<AuthUserRole> {
    // 清除用户对应的角色
    @Delete("delete from auth_user_role where user_id = #{userId}")
    void deleteByUserId(Long userId);

    void insertBatchUserRole(@Param("userRoles") List<AuthUserRole> userRoles);

    AuthUserRoleVO queryUserWithRoles(Long userId);

    // 批量删除用户和角色关系
    void deleteByUserIds(@Param("ids") List<Long> ids);

    // 删除角色和用户对应关系
    @Delete("delete from auth_user_role where role_id = #{roleId}")
    void deleteByRoleId(@Param("roleId") int id);
}




