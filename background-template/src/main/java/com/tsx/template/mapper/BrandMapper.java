package com.tsx.template.mapper;

import com.tsx.template.entity.Brand;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author Xuan
* @description 针对表【brand(品牌表)】的数据库操作Mapper
* @createDate 2024-04-04 15:47:41
* @Entity com.tsx.template.entity.Brand
*/
public interface BrandMapper extends BaseMapper<Brand> {

}




