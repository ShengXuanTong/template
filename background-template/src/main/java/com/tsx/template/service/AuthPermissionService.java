package com.tsx.template.service;

import com.tsx.template.entity.AuthPermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Xuan
* @description 针对表【auth_permission(权限)】的数据库操作Service
* @createDate 2024-04-03 12:04:02
*/
public interface AuthPermissionService extends IService<AuthPermission> {

}
