package com.tsx.template.service;

import com.tsx.template.common.vo.AuthPermissionVO;
import com.tsx.template.entity.AuthRolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Xuan
* @description 针对表【auth_role_permission(角色权限关联表)】的数据库操作Service
* @createDate 2024-04-03 12:04:02
*/
public interface AuthRolePermissionService extends IService<AuthRolePermission> {

    // 根据角色id查询权限
    AuthPermissionVO queryPermissionsByRoleId(int roleId);
}
