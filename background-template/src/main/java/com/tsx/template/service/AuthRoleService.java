package com.tsx.template.service;

import com.tsx.template.common.dto.AuthAssignPermissionDTO;
import com.tsx.template.common.vo.AuthUserRoleVO;
import com.tsx.template.entity.AuthRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author Xuan
 * @description 针对表【auth_role(角色)】的数据库操作Service
 * @createDate 2024-04-03 12:04:02
 */
public interface AuthRoleService extends IService<AuthRole> {

    // 给角色分配权限
    void assignPermission(AuthAssignPermissionDTO authAssignPermissionDTO);

    void deleteWithUser(int id);
}
