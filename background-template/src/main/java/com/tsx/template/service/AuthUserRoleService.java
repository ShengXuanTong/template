package com.tsx.template.service;

import com.tsx.template.entity.AuthUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Xuan
* @description 针对表【auth_user_role(用户角色表)】的数据库操作Service
* @createDate 2024-04-07 14:20:03
*/
public interface AuthUserRoleService extends IService<AuthUserRole> {

}
