package com.tsx.template.service;

import com.tsx.template.common.dto.AuthAssignRoleDTO;
import com.tsx.template.common.dto.AuthUserAddDTO;
import com.tsx.template.common.dto.AuthUserLoginDTO;
import com.tsx.template.common.dto.AuthUserUpdateDTO;
import com.tsx.template.common.vo.*;
import com.tsx.template.entity.AuthUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author Xuan
 * @description 针对表【auth_user】的数据库操作Service
 * @createDate 2024-04-03 12:04:22
 */
public interface AuthUserService extends IService<AuthUser> {

    AuthUserLoginVO login(AuthUserLoginDTO authUserLoginDTO);

    // 获取用户信息
    AuthUserInfoVO info();

    // 分页获取全部用户信息
    PageInfoResult<AuthUserPageItemVO> pageUser(int page, int limit, String username);

    void saveUser(AuthUser authUser);

    // 给用户赋予角色
    void assignRole(AuthAssignRoleDTO authAssignRoleDTO);

    // 查看用户包括角色
    AuthUserRoleVO queryUserWithRoles(Long userId);

    void resetPwd(Long id);

    // 添加用户
    void addUser(AuthUserAddDTO authUserAddDTO);

    // 删除用户以及旗下的角色
    void removeUserAndRoles(List<Long> ids);

    // 修改用户以及对应的角色
    void updateUserWithRole(AuthUserUpdateDTO authUserUpdateDTO);
}
