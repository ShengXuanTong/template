package com.tsx.template.service;

import com.tsx.template.entity.Brand;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Xuan
* @description 针对表【brand(品牌表)】的数据库操作Service
* @createDate 2024-04-04 15:47:41
*/
public interface BrandService extends IService<Brand> {

}
