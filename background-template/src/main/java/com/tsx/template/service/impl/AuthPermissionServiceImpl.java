package com.tsx.template.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tsx.template.entity.AuthPermission;
import com.tsx.template.service.AuthPermissionService;
import com.tsx.template.mapper.AuthPermissionMapper;
import org.springframework.stereotype.Service;

/**
* @author Xuan
* @description 针对表【auth_permission(权限)】的数据库操作Service实现
* @createDate 2024-04-03 12:04:02
*/
@Service
public class AuthPermissionServiceImpl extends ServiceImpl<AuthPermissionMapper, AuthPermission>
    implements AuthPermissionService{

}




