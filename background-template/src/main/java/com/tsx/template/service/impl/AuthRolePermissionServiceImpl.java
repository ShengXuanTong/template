package com.tsx.template.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tsx.template.common.vo.AuthPermissionVO;
import com.tsx.template.entity.AuthRolePermission;
import com.tsx.template.service.AuthRolePermissionService;
import com.tsx.template.mapper.AuthRolePermissionMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author Xuan
* @description 针对表【auth_role_permission(角色权限关联表)】的数据库操作Service实现
* @createDate 2024-04-03 12:04:02
*/
@Service
@RequiredArgsConstructor
public class AuthRolePermissionServiceImpl extends ServiceImpl<AuthRolePermissionMapper, AuthRolePermission>
    implements AuthRolePermissionService{

    final AuthRolePermissionMapper authRolePermissionMapper;
    @Override
    public AuthPermissionVO queryPermissionsByRoleId(int roleId) {
        List<Integer> ids = authRolePermissionMapper.queryPermissionsByRoleId(roleId);
        return new AuthPermissionVO(ids);
    }
}




