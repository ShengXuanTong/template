package com.tsx.template.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tsx.template.entity.AuthUserRole;
import com.tsx.template.service.AuthUserRoleService;
import com.tsx.template.mapper.AuthUserRoleMapper;
import org.springframework.stereotype.Service;

/**
* @author Xuan
* @description 针对表【auth_user_role(用户角色表)】的数据库操作Service实现
* @createDate 2024-04-07 14:20:03
*/
@Service
public class AuthUserRoleServiceImpl extends ServiceImpl<AuthUserRoleMapper, AuthUserRole>
    implements AuthUserRoleService{

}




