package com.tsx.template.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tsx.template.entity.Brand;
import com.tsx.template.service.BrandService;
import com.tsx.template.mapper.BrandMapper;
import org.springframework.stereotype.Service;

/**
* @author Xuan
* @description 针对表【brand(品牌表)】的数据库操作Service实现
* @createDate 2024-04-04 15:47:41
*/
@Service
public class BrandServiceImpl extends ServiceImpl<BrandMapper, Brand>
    implements BrandService{

}




