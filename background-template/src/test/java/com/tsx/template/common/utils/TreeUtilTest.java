package com.tsx.template.common.utils;

import com.tsx.template.common.vo.PermissionVO;
import com.tsx.template.entity.AuthPermission;
import com.tsx.template.service.AuthPermissionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TreeUtilTest {

    @Autowired
    public AuthPermissionService authPermissionService;

    @Test
    void treeMenu() {
        List<AuthPermission> list = authPermissionService.list();
        List<PermissionVO> permissionVOS = TreeUtil.TreeMenu(list, 0);
        for (PermissionVO permissionVO : permissionVOS) {
            System.out.println("permissionVO = " + permissionVO);
        }
    }
}