import { ResponseData } from "@/api/type"
import request from "@/utils/request"
import { PermissionIds, Tree } from "./type"

enum API {
    BASE_URL = '/permission'
}

export const reqGetPermission = () => {
    return request.get<any, ResponseData<Tree[]>>(API.BASE_URL)
}

export const reqPermissionByRoleId = (roleId: number) => {
    return request.get<any, ResponseData<PermissionIds>>(`${API.BASE_URL}/${roleId}`)
}