export interface Tree {
    id: number
    name: string
    code: string
    children: Tree[]
}
export interface AssignPermission {
    roleId: number
    permissions: number[]
}

export interface PermissionIds {
    permissions: number[]
}