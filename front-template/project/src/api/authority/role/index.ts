import { PageInfo, ResponseData } from '@/api/type.ts'
import request from '@/utils/request.ts'
import { AssignPerItem, Role, ResponsePageRole } from '@/api/authority/role/type.ts'

enum API {
    BASE_URL = '/role',
    ASSIGN_URL = '/role/assignPermission',
}

// 分页查询角色信息
export const reqPageRole = (pageInfo: PageInfo, roleName: string) => {
    return request.get<any, ResponsePageRole>(
        `${API.BASE_URL}/${pageInfo.page}/${pageInfo.limit}`,
        { params: { roleName } },
    )
}

// 查询所有角色
export const reqRoles = () => {
    return request.get<any, ResponseData<Role[]>>(API.BASE_URL)
}

// 删除角色
export const reqDeleteRole = (id: number) => {
    return request.delete<any, ResponseData<null>>(`${API.BASE_URL}/${id}`)
}

// 添加角色
export const reqAddRole = (role: Role) => {
    const { code, remark } = role
    return request.post<any, ResponseData<null>>(API.BASE_URL, { code, remark })
}

// 更新角色
export const reqUpdateRole = (role: Role) => {
    const { id, code, remark } = role
    return request.put<any, ResponseData<null>>(API.BASE_URL, { id, code, remark })
}

// 分配权限
export const reqAssignPermission = (assignPerItem: AssignPerItem) => {
    return request.put<any, ResponseData<null>>(`${API.BASE_URL}/assignPermission`, assignPerItem)
}

// 查询单个用户信息
export const reqQueryRoleById = (id: number) => {
    return request.get<any, ResponseData<Role>>(`${API.BASE_URL}/${id}`)
}