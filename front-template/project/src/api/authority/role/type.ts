import { ResponseData, ResponsePageResult } from "@/api/type"

export interface Role {
    id: number
    code: string
    remark: string
    createTime?: string
    updateTime?: string
}

export interface AssignPerItem {
    roleId: number
    permissions: number[]
}
export type ResponsePageRole = ResponseData<ResponsePageResult<Role>>
