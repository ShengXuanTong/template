// 统一管理用户相关接口
import request from '@/utils/request.ts'
import type {
    LoginResponse,
    LoginUser,
    UserInfoResponse,
    UserItem,
    UserPageResponse,
} from './type.ts'
import { PageInfo, ResponseData } from '../../type.ts'
import { AssignRole, UserWithRole } from './type.ts'

// 枚举接口地址
enum API {
    LOGIN_URL = '/user/login',
    USERINFO_URL = '/user/info',
    LOGOUT_URL = '/user/logout',
    BASE_URL = '/user',
    ASSIGN_ROLE_URL = '/user/assignRole',
    RESET_PASSWORD_URL = '/user/resetPwd'
}

// 暴露请求函数
// 登录请求
export const reqLogin = (data: LoginUser) => {
    return request.post<any, LoginResponse>(API.LOGIN_URL, data)
}
// 获取用户信息
export const reqUserInfo = () => {
    return request.get<any, UserInfoResponse>(API.USERINFO_URL)
}
// 退出登录
export const reqLogout = () => {
    return request.get<any, any>(API.LOGOUT_URL)
}

// 分页查询全部用户信息
export const reqUserPage = (pageInfo: PageInfo, username: string) => {
    return request.get<any, ResponseData<UserPageResponse>>(
        `${API.BASE_URL}/${pageInfo.page}/${pageInfo.limit}`,
        { params: { username } },
    )
}

// 删除
export const reqDeleteUser = (ids: number[]) => {
    return request.delete<any, ResponseData<null>>(API.BASE_URL, { data: ids })
}
// 获取用户信息以及角色
export const reqUserWithRoles = (id: number) => {
    return request.get<any, ResponseData<UserWithRole>>(`${API.BASE_URL}/${id}`)
}

// 新增用户
export const reqAddUser = (user: UserItem) => {
    return request.post<any, ResponseData<null>>(API.BASE_URL, user)
}

// 修改
export const reqUpdateUser = (user: UserItem) => {
    return request.put<any, ResponseData<null>>(API.BASE_URL, user)
}

// 分配角色
export const reqAssignRole = (assignRoleUser: AssignRole) => {
    return request.put<any, ResponseData<null>>(
        API.ASSIGN_ROLE_URL,
        assignRoleUser,
    )
}

// 重置密码
export const resetPassword = (id: number) => {
    return request.put<any, ResponseData<null>>(`${API.RESET_PASSWORD_URL}/${id}`)
}