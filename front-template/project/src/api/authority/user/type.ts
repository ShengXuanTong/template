import { ResponseData, ResponsePageResult } from '@/api/type.ts'
import { Role } from '@/api/authority/role/type.ts'

// 登录请求参数
export interface LoginUser {
    username: string
    password: string
}

// 用户信息
interface UserInfo {
    avatar: string
    nickName: string
    routes: string[]
    buttons: string[]
    roles: string[]
}

// 分页的用户Item
export interface UserPageItem {
    id: number
    username: string
    nickName: string
    roles: string
    status: number
    createTime: string
}

// 用户的信息加上角色
export interface UserWithRole {
    id: number
    username: string
    nickName: string
    status: number
    roles: Role[]
    avatar: string
}

export interface UserItem {
    id: number
    username: string
    nickName: string
    roles: number[]
    status: number
    avatar: string
    password: string
}

export interface AssignRole {
    userId: number
    roles: number[]
}

// 登录返回的信息
export type LoginResponse = ResponseData<{ token: string }>

// 获取用户信息的信息
export type UserInfoResponse = ResponseData<UserInfo>

// 用户信息分页对象
export type UserPageResponse = ResponsePageResult<UserPageItem>
