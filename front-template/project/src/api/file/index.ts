import request from '@/utils/request.ts'
import { ResponseData } from '@/api/type.ts'

export const reqFileUpload = (fd: FormData) => {
    return request.post<any, ResponseData<string>>('/file/upload', fd)
}
