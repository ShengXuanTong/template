import request from '@/utils/request.ts'
import { PageInfo, ResponseData } from '@/api/type.ts'
import { Brand, ResponsePageBrand } from '@/api/product/brand/type.ts'

enum API {
    BASE_URL = '/product/brand',
}

export const reqBrandPage = (pageInfo: PageInfo) => {
    return request.get<any, ResponsePageBrand>(
        `${API.BASE_URL}/${pageInfo.page}/${pageInfo.limit}`,
    )
}

export const reqBrandQuery = (id: number) => {
    return request.get<any, ResponseData<Brand>>(`${API.BASE_URL}/${id}`)
}

export const reqUpdateBrand = (data: Brand) => {
    return request.put<ResponseData<any>>(API.BASE_URL, data)
}

export const reqSaveBrand = (data: Brand) => {
    return request.post<ResponseData<any>>(API.BASE_URL, data)
}
export const reqRemoveBrand = (id: number) => {
    return request.delete(`${API.BASE_URL}/${id}`)
}
