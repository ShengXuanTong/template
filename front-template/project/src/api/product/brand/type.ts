import { ResponseData, ResponsePageResult } from '@/api/type.ts'

export interface Brand {
    id: number
    name: string
    logoUrl: string
}

export type ResponsePageBrand = ResponseData<ResponsePageResult<Brand>>
