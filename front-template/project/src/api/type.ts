// 返回的result结果
export interface ResponseData<T> {
    code: number
    data: T
    message: string
}

export interface ResponsePageResult<T> {
    records: T[]
    current: number
    total: number
}

export type PageInfo = {
    page: number
    limit: number
}
