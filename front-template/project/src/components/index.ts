// 引入
import SvgIcon from '@/components/SvgIcon/index.vue'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import { Menu } from '@element-plus/icons-vue'
import type { App, Component } from 'vue'
// 定义组件对象
const components: { [name: string]: Component } = { SvgIcon }
// 导出自定义插件
export default {
    install(app: App) {
        // 注册自定义组件
        Object.keys(components).forEach((key: string) => {
            app.component(key, components[key])
        })
        // 注册 Element Plus 图标组件
        for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
            app.component(key, component)
        }
        app.component('MenuIcon', Menu)
    },
}
