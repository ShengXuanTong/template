import useUserStore from "@/store/modules/user";
import { storeToRefs } from "pinia";
import { App, DirectiveBinding } from "vue";
const userStore = useUserStore()
export default function hasPermission(app: App<Element>) {
    app.directive('permission', {
        mounted(el: any, binding: DirectiveBinding) {
            const { value } = binding
            let { buttons } = storeToRefs(userStore)
            if (!buttons.value.includes(value)) {
                el.style.display = 'none'
            } else {
                el.style.display = 'auto'
            }
        }
    })
}