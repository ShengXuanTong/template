// 封装：切换全屏
import screenfull from 'screenfull'
import { ref } from 'vue'

export default function useScreenFull() {
    const isFullScreenTag = ref(false)
    const handleFullScreen = () => {
        if (screenfull.isEnabled) {
            // 检测当前是否全屏，如果是全屏就退出，否则就全屏
            if (screenfull.isFullscreen) {
                console.log('全屏')
                screenfull.toggle()
                isFullScreenTag.value = false
            } else {
                console.log('111全屏')
                // 进入全屏
                screenfull.toggle()
                isFullScreenTag.value = true
            }
        } else {
            alert('提示：不支持切换全屏。')
        }
    }
    return { isFullScreenTag, handleFullScreen }
}
