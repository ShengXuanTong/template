import { createApp } from 'vue'
import App from '@/App.vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
//eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
// svg
import 'virtual:svg-icons-register'
// 引入自定义插件对象：注册整个项目全局组件
import globalComponents from '@/components/index'
// 引入模板的全局样式
import '@/styles/index.less'
// 引入路由
import router from '@/router'
// 引入pinia
import store from '@/store'
// 引入permission
import '@/permisstion.ts'
// 暗黑模式
import 'element-plus/theme-chalk/dark/css-vars.css'
// setting
import useSettingStore from '@/store/modules/setting.ts'
// 引入自定义指令
import hasPermission from './directive/hasPermission'

const app = createApp(App)
app.use(ElementPlus, {
    locale: zhCn,
})
// 使用插件
app.use(globalComponents)
app.use(router)
app.use(store)
hasPermission(app)
app.mount('#app')
const settingStore = useSettingStore()
// 监听isDark数据
settingStore.$subscribe((mutation, state) => {
    const html = document.documentElement
    html.className = state.isDark ? 'dark' : ''
})
// 使用import.meta.env可以获得变量
// console.log(import.meta.env)
