// 常量路由
import { RouteRecordRaw } from 'vue-router'

export const constantRoutes: RouteRecordRaw[] = [
    {
        name: 'login',
        path: '/login',
        component: () => import('@/views/login/index.vue'),
        meta: {
            title: '登录',
            hidden: true,
            icon: 'User',
        },
    },
    {
        // 一级
        name: 'layout',
        path: '/',
        component: () => import('@/layout/index.vue'),
        redirect: '/home',
        meta: {
            title: 'layout',
            hidden: false,
            icon: 'HomeFilled',
        },
        // 二级，若只有一个，就只展示第一个
        children: [
            {
                name: 'home',
                path: '/home',
                component: () => import('@/views/home/index.vue'),
                meta: {
                    title: '首页',
                    hidden: false,
                    icon: 'HomeFilled',
                },
            },
        ],
    },
    {
        // 一级
        name: 'userInfo',
        path: '/userInfo',
        component: () => import('@/layout/index.vue'),
        redirect: '/userInfo/userCenter',
        meta: {
            title: 'layout',
            hidden: false,
            icon: 'HomeFilled',
        },
        children: [
            {
                name: 'userCenter',
                path: '/userInfo/userCenter',
                component: () => import('@/views/userCenter/index.vue'),
                meta: {
                    title: '个人中心',
                    hidden: true,
                    icon: 'User'
                }
            }
        ]
    },
    // {
    //     path: '/screen',
    //     name: 'screen',
    //     component: () => import('@/views/screen/index.vue'),
    //     meta: {
    //         hidden: true,
    //         title: '数据大屏',
    //         icon: 'DataLine',
    //     },
    // },


    {
        name: '404',
        path: '/404',
        component: () => import('@/views/404/index.vue'),
        meta: {
            title: '404',
            hidden: true,
            icon: 'Warning',
        },
    },
]

// 异步路由
export const asyncRoutes: RouteRecordRaw[] = [
    {
        name: 'authority',
        path: '/authority',
        component: () => import('@/layout/index.vue'),
        meta: {
            title: '权限管理',
            hidden: false,
            icon: 'Setting',
        },
        redirect: '/authority/user',
        children: [
            {
                name: 'user',
                path: '/authority/user',
                component: () => import('@/views/authority/user/index.vue'),
                meta: {
                    title: '用户管理',
                    hidden: false,
                    icon: 'User',
                },
            },
            {
                name: 'role',
                path: '/authority/role',
                component: () => import('@/views/authority/role/index.vue'),
                meta: {
                    title: '角色管理',
                    hidden: false,
                    icon: 'UserFilled',
                },
            },
            {
                name: 'menu',
                path: '/authority/permission',
                component: () =>
                    import('@/views/authority/permission/index.vue'),
                meta: {
                    title: '菜单管理',
                    hidden: false,
                    icon: 'MenuIcon',
                },
            },
        ],
    },
    {
        name: 'product',
        path: '/product',
        component: () => import('@/layout/index.vue'),
        redirect: '/product/brand',
        meta: {
            title: '商品管理',
            hidden: false,
            icon: 'Goods',
        },
        children: [
            {
                name: 'brand',
                path: '/product/brand',
                component: () => import('@/views/product/brand/index.vue'),
                meta: {
                    title: '品牌管理',
                    hidden: false,
                    icon: 'ShoppingCartFull',
                },
            },
            {
                name: 'attr',
                path: '/product/attr',
                component: () => import('@/views/product/attr/index.vue'),
                meta: {
                    title: '属性管理',
                    hidden: false,
                    icon: 'Key',
                },
            },
            {
                name: 'sku',
                path: '/product/sku',
                component: () => import('@/views/product/sku/index.vue'),
                meta: {
                    title: 'sku管理',
                    hidden: false,
                    icon: 'Operation',
                },
            },
            {
                name: 'spu',
                path: '/product/spu',
                component: () => import('@/views/product/spu/index.vue'),
                meta: {
                    title: 'spu管理',
                    hidden: false,
                    icon: 'Operation',
                },
            },
        ],
    },

]

// 任意路由
export const anyRoute: RouteRecordRaw = {
    name: 'any',
    path: '/:pathMatch(.*)*',
    redirect: '/404',
    meta: {
        title: 'any',
        hidden: true,
    },
}