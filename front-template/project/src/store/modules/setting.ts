import { defineStore } from 'pinia'

const useSettingStore = defineStore('Setting', {
    state() {
        return {
            isCollapse: false, //用于控制菜单折叠
            refresh: false, //用于设置是否刷新
            isDark: false
        }
    },
    actions: {
        changeIsCollapse() {
            this.isCollapse = !this.isCollapse
        },
        changeRefresh() {
            this.refresh = !this.refresh
        },
    },
})

export default useSettingStore
