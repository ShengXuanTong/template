import { FormInstance } from 'element-plus'
import { Ref } from 'vue'

// export function formClearValidate(form: Ref<FormInstance | undefined>) {
//     form.value?.clearValidate()
// }
//
// export function formResetField(form: Ref<FormInstance | undefined>) {
//     form.value?.resetFields()
// }

export function resetForm(form: Ref<FormInstance | undefined>) {
    form.value?.resetFields()
    form.value?.clearValidate()
}
