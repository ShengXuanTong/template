// 进行axios二次封装：使用拦截器
import axios from 'axios'
import { ElMessage } from 'element-plus'
import useUserStore from '@/store/modules/user.ts'
import router from '@/router'

/**
 * 参数处理
 * @param {*} params  参数
 */
export function tansParams(params: any) {
    let result = ''
    for (const propName of Object.keys(params)) {
        const value = params[propName]
        const part = encodeURIComponent(propName) + '='
        if (value !== null && value !== '' && typeof value !== 'undefined') {
            if (typeof value === 'object') {
                for (const key of Object.keys(value)) {
                    if (
                        value[key] !== null &&
                        value[key] !== '' &&
                        typeof value[key] !== 'undefined'
                    ) {
                        const params = propName + '[' + key + ']'
                        const subPart = encodeURIComponent(params) + '='
                        result += subPart + encodeURIComponent(value[key]) + '&'
                    }
                }
            } else {
                result += part + encodeURIComponent(value) + '&'
            }
        }
    }
    return result
}

// 利用axios的create方法，创建一个实例
const request = axios.create({
    // 基础路径
    baseURL: import.meta.env.VITE_APP_BASE_API,
    timeout: 3000,
})
// 配置请求拦截器
request.interceptors.request.use(
    (config) => {
        const token = useUserStore().token
        token && (config.headers.satoken = token)
        // 如果是 GET 请求,处理请求参数
        // get请求映射params参数
        if (config.method === 'get' && config.params) {
            let url = config.url + '?' + tansParams(config.params)
            url = url.slice(0, -1)
            config.params = {}
            config.url = url
        }
        return config
    },
    (error) => {
        return Promise.reject(error)
    },
)

// 配置响应拦截器
// error处理网络错误
// response处理后端返回的错误或者是简化结果
request.interceptors.response.use(
    (response) => {
        const { data } = response
        if (data.code === 4001) {
            // 这里已经做了退出登录了,把提示交出去
            useUserStore().resetToken()
            router.push({
                path: '/login',
                query: { redirect: router.currentRoute.value.path },
            })
            return Promise.reject('无效的会话，或者会话已过期，请重新登录')
        } else if (data.code === 4003) {
            // 无权限的拦截
            ElMessage({
                message: data.message,
                type: 'warning',
            })
            return Promise.reject(data.message)
        } else if (data.code === 4005) {
            ElMessage({
                message: data.message,
                type: 'warning',
            })
            return Promise.reject(data.message)
        } else if (data.code === 5000) {
            ElMessage.error(data.message)
            return Promise.reject(data.message)
        }
        return response.data
    },
    (error) => {
        // 处理http网络错误
        // 定义一个变量，存储错误信息
        let message = ''
        // http状态码
        console.log(error)
        if (error.response) {
            // 请求成功发出且服务器也响应了状态码，但状态代码超出了 2xx 的范围
            const status = error.response.status
            switch (status) {
                case 401:
                    message = 'token过期'
                    break
                case 403:
                    message = '无权访问'
                    break
                case 404:
                    message = '请求地址错误'
                    break
                case 500:
                    message = '服务器故障稍后重试'
                    break
                default:
                    message = '网络故障'
                    break
            }
            useUserStore().resetToken()
        } else if (error.request) {
            // 请求成功发起，没有收到响应
            message = '服务器繁忙请稍后重试'
        }
        ElMessage({
            type: 'error',
            message,
            showClose: true,
        })
        return Promise.reject(message)
    },
)

// 暴露
export default request
