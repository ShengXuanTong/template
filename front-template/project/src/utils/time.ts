export default function getTime() {
    let message: string
    const currentHour = new Date().getHours()
    if (currentHour < 12) {
        message = '上午'
    } else if (currentHour < 18) {
        message = '下午'
    } else {
        message = '晚上'
    }
    return message
}
