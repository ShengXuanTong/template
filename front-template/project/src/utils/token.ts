import setting from '@/setting'

export const SET_TOKEN = (token: string) => {
    localStorage.setItem(setting.tokenName, token)
}

export const GET_TOKEN = () => {
    return localStorage.getItem(setting.tokenName)
}

export const REMOVE_TOKEN = () => {
    localStorage.removeItem(setting.tokenName)
}
