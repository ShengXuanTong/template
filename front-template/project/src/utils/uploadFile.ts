import { reqFileUpload } from '@/api/file'
import { UploadRequestOptions } from 'element-plus'

export default function uploadFile(
    fileObject: UploadRequestOptions,
): Promise<any> {
    return new Promise((resolve, reject) => {
        const fd = new FormData()
        fd.append('file', fileObject.file)
        reqFileUpload(fd)
            .then((res) => {
                resolve(res.data)
            })
            .catch((err) => {
                reject(err)
            })
    })
}
