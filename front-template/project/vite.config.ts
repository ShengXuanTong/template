import { ConfigEnv, loadEnv, UserConfigExport } from 'vite'
import vue from '@vitejs/plugin-vue'
import { createSvgIconsPlugin } from 'vite-plugin-svg-icons'
import path from 'path'
import { viteMockServe } from 'vite-plugin-mock'
import VueSetupExtend from 'vite-plugin-vue-setup-extend'
// https://vitejs.dev/config/
export default ({ command, mode }: ConfigEnv): UserConfigExport => {
    const env = loadEnv(mode, process.cwd())
    return {
        plugins: [
            vue(),
            createSvgIconsPlugin({
                iconDirs: [path.resolve(process.cwd(), 'src/assets/icons')],
                symbolId: 'icon-[dir]-[name]',
            }),
            viteMockServe({
                mockPath: 'mock',
                enable: command === 'serve', //保证开发阶段可以使用mock接口
            }),
            VueSetupExtend(),
        ],
        resolve: {
            alias: {
                '@': path.resolve('./src'), // 相对路径别名配置，使用 @ 代替 src
            },
        },
        // less全局变量的一个配置
        css: {
            // css预处理器
            preprocessorOptions: {
                less: {
                    // charset: false,
                    javascriptEnabled: true,
                    additionalData: '@import "./src/styles/variable.less";',
                },
            },
        },
        // 代理跨域
        server: {
            proxy: {
                // 正则表达式写法,开头
                [env.VITE_APP_BASE_API]: {
                    // 获取服务器地址
                    target: env.VITE_SERVER,
                    changeOrigin: true,
                    rewrite: (path) => path.replace(/^\/api/, ''), // 不可以省略rewrite
                },
            },
        },
    }
}
