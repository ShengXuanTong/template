/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80033
 Source Host           : localhost:3306
 Source Schema         : template

 Target Server Type    : MySQL
 Target Server Version : 80033
 File Encoding         : 65001

 Date: 14/04/2024 16:14:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '权限名字',
  `parent_id` int NOT NULL DEFAULT 0 COMMENT '父id',
  `type` tinyint NOT NULL COMMENT '权限类型 0菜单 1操作',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '操作标识符',
  `menu_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '菜单路由',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '权限' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO `auth_permission` VALUES (1, '权限管理', 0, 0, NULL, '/authority', '2024-04-02 21:45:18', '2024-04-02 21:45:18');
INSERT INTO `auth_permission` VALUES (2, '用户管理', 1, 0, NULL, '/authority/user', '2024-04-02 21:46:22', '2024-04-02 21:46:22');
INSERT INTO `auth_permission` VALUES (3, '角色管理', 1, 0, NULL, '/authority/role', '2024-04-02 21:46:22', '2024-04-02 21:46:22');
INSERT INTO `auth_permission` VALUES (4, '菜单管理', 1, 0, NULL, '/authority/permission', '2024-04-02 21:46:49', '2024-04-02 21:46:49');
INSERT INTO `auth_permission` VALUES (5, '添加用户', 2, 1, 'btn.user.add', NULL, '2024-04-02 21:49:16', '2024-04-02 21:49:16');
INSERT INTO `auth_permission` VALUES (6, '删除用户', 2, 1, 'btn.user.delete', NULL, '2024-04-02 21:49:48', '2024-04-02 21:49:48');
INSERT INTO `auth_permission` VALUES (7, '修改用户', 2, 1, 'btn.user.update', NULL, '2024-04-02 21:50:15', '2024-04-02 21:50:15');
INSERT INTO `auth_permission` VALUES (8, '分配角色', 2, 1, 'btn.user.assign', NULL, '2024-04-02 21:51:44', '2024-04-02 21:51:44');
INSERT INTO `auth_permission` VALUES (10, '添加角色', 3, 1, 'btn.role.add', NULL, '2024-04-02 21:58:56', '2024-04-02 21:58:56');
INSERT INTO `auth_permission` VALUES (11, '删除角色', 3, 1, 'btn.role.delete', NULL, '2024-04-02 21:58:56', '2024-04-02 21:58:56');
INSERT INTO `auth_permission` VALUES (12, '修改角色', 3, 1, 'btn.role.update', NULL, '2024-04-02 21:58:56', '2024-04-02 21:58:56');
INSERT INTO `auth_permission` VALUES (13, '分配权限', 3, 1, 'btn.role.assign', NULL, '2024-04-02 21:58:56', '2024-04-02 21:58:56');
INSERT INTO `auth_permission` VALUES (17, '商品管理', 0, 0, NULL, '/product', '2024-04-05 15:02:32', '2024-04-05 15:02:32');
INSERT INTO `auth_permission` VALUES (18, '品牌管理', 17, 0, NULL, '/product/brand', '2024-04-05 15:03:56', '2024-04-05 15:03:56');
INSERT INTO `auth_permission` VALUES (19, '添加品牌', 18, 1, 'btn.brand.add', NULL, '2024-04-05 15:05:37', '2024-04-05 15:05:37');
INSERT INTO `auth_permission` VALUES (20, '修改品牌', 18, 1, 'btn.brand.update', NULL, '2024-04-05 15:05:37', '2024-04-05 15:05:37');
INSERT INTO `auth_permission` VALUES (21, '删除品牌', 18, 1, 'btn.brand.delete', NULL, '2024-04-05 15:05:37', '2024-04-05 15:05:37');
INSERT INTO `auth_permission` VALUES (22, '查找某个品牌', 18, 1, 'btn.brand.select', NULL, '2024-04-06 21:41:59', '2024-04-06 21:41:59');

-- ----------------------------
-- Table structure for auth_role
-- ----------------------------
DROP TABLE IF EXISTS `auth_role`;
CREATE TABLE `auth_role`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色名[后端校验]',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '详解说明[前台展示]',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_role
-- ----------------------------
INSERT INTO `auth_role` VALUES (1, 'admin', '管理员', '2024-04-02 21:17:06', '2024-04-02 21:17:06');
INSERT INTO `auth_role` VALUES (2, 'common', '普通用户', '2024-04-02 21:17:21', '2024-04-07 18:11:35');

-- ----------------------------
-- Table structure for auth_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_role_permission`;
CREATE TABLE `auth_role_permission`  (
  `role_id` int NOT NULL COMMENT '角色id',
  `permission_id` int NOT NULL COMMENT '权限id',
  PRIMARY KEY (`role_id`, `permission_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色权限关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_role_permission
-- ----------------------------
INSERT INTO `auth_role_permission` VALUES (1, 1);
INSERT INTO `auth_role_permission` VALUES (1, 2);
INSERT INTO `auth_role_permission` VALUES (1, 3);
INSERT INTO `auth_role_permission` VALUES (1, 4);
INSERT INTO `auth_role_permission` VALUES (1, 5);
INSERT INTO `auth_role_permission` VALUES (1, 6);
INSERT INTO `auth_role_permission` VALUES (1, 7);
INSERT INTO `auth_role_permission` VALUES (1, 8);
INSERT INTO `auth_role_permission` VALUES (1, 10);
INSERT INTO `auth_role_permission` VALUES (1, 11);
INSERT INTO `auth_role_permission` VALUES (1, 12);
INSERT INTO `auth_role_permission` VALUES (1, 13);
INSERT INTO `auth_role_permission` VALUES (1, 17);
INSERT INTO `auth_role_permission` VALUES (1, 18);
INSERT INTO `auth_role_permission` VALUES (1, 19);
INSERT INTO `auth_role_permission` VALUES (1, 20);
INSERT INTO `auth_role_permission` VALUES (1, 21);
INSERT INTO `auth_role_permission` VALUES (1, 22);
INSERT INTO `auth_role_permission` VALUES (2, 17);
INSERT INTO `auth_role_permission` VALUES (2, 18);
INSERT INTO `auth_role_permission` VALUES (2, 19);
INSERT INTO `auth_role_permission` VALUES (2, 20);

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `username` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名',
  `password` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  `nick_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '昵称',
  `avatar` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户头像',
  `status` tinyint NOT NULL DEFAULT 0 COMMENT '启用 1 禁用 0',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_user_pk`(`username` ASC) USING BTREE COMMENT '用户名索引',
  INDEX `auth_user_username_index`(`username` ASC) USING BTREE COMMENT '用户名索引'
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_user
-- ----------------------------
INSERT INTO `auth_user` VALUES (1, 'admin', '7c4a8d09ca3762af61e59520943dc26494f8941b', '管理员', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 1, '2024-04-02 21:12:15', '2024-04-14 14:48:07');
INSERT INTO `auth_user` VALUES (5, 'wangwu', '7c4a8d09ca3762af61e59520943dc26494f8941b', '王五', 'http://localhost:8081/file/download/c4258450-be37-40d9-a621-170dcc9a96f7_荷花.jpg', 1, '2024-04-05 15:01:21', '2024-04-14 14:48:54');
INSERT INTO `auth_user` VALUES (20, 'lisi', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'lisi', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 1, '2024-04-10 13:58:10', '2024-04-11 15:42:27');

-- ----------------------------
-- Table structure for auth_user_role
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_role`;
CREATE TABLE `auth_user_role`  (
  `user_id` bigint NOT NULL COMMENT '用户id',
  `role_id` int NOT NULL COMMENT '角色id',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_user_role
-- ----------------------------
INSERT INTO `auth_user_role` VALUES (1, 1);
INSERT INTO `auth_user_role` VALUES (5, 2);
INSERT INTO `auth_user_role` VALUES (20, 2);

-- ----------------------------
-- Table structure for brand
-- ----------------------------
DROP TABLE IF EXISTS `brand`;
CREATE TABLE `brand`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '品牌名字',
  `logo_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'logo路径',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '品牌表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of brand
-- ----------------------------
INSERT INTO `brand` VALUES (1, '小米', 'http://localhost:8081/file/download/xiaomi.png', '2024-04-04 15:46:08', '2024-04-04 20:54:31');
INSERT INTO `brand` VALUES (2, '阿迪达斯', 'http://localhost:8081/file/download/adidas.png', '2024-04-04 15:46:22', '2024-04-04 15:46:22');
INSERT INTO `brand` VALUES (3, '蜜雪冰城', 'http://localhost:8081/file/download/mxbc.png', '2024-04-04 15:46:42', '2024-04-04 15:46:42');
INSERT INTO `brand` VALUES (4, '苹果', 'http://localhost:8081/file/download/Apple.png', '2024-04-04 15:46:55', '2024-04-04 15:46:55');
INSERT INTO `brand` VALUES (5, '耐克', 'http://localhost:8081/file/download/nike.png', '2024-04-04 15:47:10', '2024-04-04 15:47:10');
INSERT INTO `brand` VALUES (6, '瑞幸', 'http://localhost:8081/file/download/ruixing.png', '2024-04-04 15:47:24', '2024-04-04 15:47:24');
INSERT INTO `brand` VALUES (7, '飞跃', 'http://localhost:8081/file/download/feiyue.png', '2024-04-04 20:11:29', '2024-04-04 23:56:48');
INSERT INTO `brand` VALUES (8, '古驰', 'http://localhost:8081/file/download/guchi.png', '2024-04-04 20:11:29', '2024-04-04 20:11:29');
INSERT INTO `brand` VALUES (9, '李宁', 'http://localhost:8081/file/download/lining.png', '2024-04-04 20:11:29', '2024-04-04 20:11:29');
INSERT INTO `brand` VALUES (10, '安踏', 'http://localhost:8081/file/download/anta.png', '2024-04-04 20:11:29', '2024-04-04 20:11:29');
INSERT INTO `brand` VALUES (23, '花花公子', 'http://localhost:8081/file/download/384b0e45-3e5a-4820-b3a3-11003c9ca22c_hhgz.png', '2024-04-06 18:55:13', '2024-04-06 18:55:12');
INSERT INTO `brand` VALUES (24, '贵人鸟', 'http://localhost:8081/file/download/6892026b-f9b8-4867-a5d8-9fcfda4639bb_grn.png', '2024-04-06 18:55:22', '2024-04-06 18:55:21');

SET FOREIGN_KEY_CHECKS = 1;
